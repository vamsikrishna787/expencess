import React, {useContext} from 'react';
import {createBrowserRouter} from 'react-router-dom';
import {lazy,Suspense} from 'react';
import RootLayout from  '../layouts/RootLayout';
import Error from '../components/Error';
import Progress from '../components/Progress';
import {RouterProvider,redirect} from 'react-router-dom';
import { AuthContext } from '../../shared/contextstore/authContext';


const Home =lazy(()=>import('../../pages/home/Home'));
const Expence =lazy(()=>import('../../pages/expence/Expence'));
const Login =lazy(()=>import('../../pages/login/Login'));
const Register =lazy(()=>import('../../pages/register/Register'));
const Search =lazy(()=>import('../../pages/search/Search'));
const SearchDetails =lazy(()=>import('../../pages/search/searchDetails'));



const RooterMaster:React.FC =()=>{
  const { authData} = useContext(AuthContext);
  const routes=createBrowserRouter([
    { path:'/',element:<RootLayout/>,errorElement:<Error/>,children:[
     
      {index:true,element: <Suspense fallback={<Progress/>}><Home/></Suspense>,
       loader: (meta)=> authData.isLoggedIn ? import('../../pages/home/Home').then((module)=>module.default):redirect('/login')},

       {path:'login',element:<Suspense fallback={<Progress/>}><Login/></Suspense>,
       loader:(meta)=> !authData.isLoggedIn ? import('../../pages/login/Login').then((module)=>module.default):redirect('/')},
      
       {path:'register',element:<Suspense fallback={<Progress/>}><Register/></Suspense>,
       loader:(meta)=> !authData.isLoggedIn ? import('../../pages/register/Register').then((module)=>module.default):redirect('/')},

       {path:'expence',element:  <Suspense fallback={<Progress/>}><Expence/></Suspense>,
       loader:(meta)=> authData.isLoggedIn ?  import('../../pages/expence/Expence').then((module)=>module.default):redirect('/login')},
  
       {path:'search',element:  <Suspense fallback={<Progress/>}><Search/></Suspense>,
       loader:(meta)=>   import('../../pages/search/Search').then((module)=>module.default)},

       {path:'search/:expenceId',element:  <Suspense fallback={<Progress/>}><SearchDetails/></Suspense>,
       loader:(meta)=>  import('../../pages/search/searchDetails').then((module)=>module.default)},
      ]
    } 
  ]);
  return <RouterProvider router={routes}/>
}
export default RooterMaster;