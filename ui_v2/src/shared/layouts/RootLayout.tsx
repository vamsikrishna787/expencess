
import MainNavigation from './MainNavigation'
import { Outlet } from "react-router-dom";
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';

const RootLayout: React.FC = () => {

    return <>
            <MainNavigation />
                <Box sx={{ bgcolor: 'background.paper', pt: 8, pb: 6, }}>
                    <Container maxWidth="lg">
                        <main>
                            <Outlet />
                        </main>
                    </Container>
                </Box>        
    </>
}
export default RootLayout;
