import axios, { AxiosResponse } from 'axios';
import getConfig from '../../appSettings';
import { RegisterModelUI } from '../uiModels/RegisterModelUI';


//import { TokenService } from './GetToken';
//const service = new TokenService();

// const token = service.getToken();
// axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;

export  class RegisterService {
    public async postData(model:RegisterModelUI): Promise< AxiosResponse<void>> { 
        return await axios.post(`${getConfig()}WeatherForecast/register`, model);
    }
}