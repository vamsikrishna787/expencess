import { AuthInfo } from "../contextstore/authContext";



export  class TokenService {
    
    public  getToken(): string { 
        const storedData = localStorage.getItem("person");
         const myObject: AuthInfo = JSON.parse(storedData!); 
        return myObject.token!;
    }
}