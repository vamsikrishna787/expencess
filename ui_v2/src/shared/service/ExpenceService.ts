import axios, { AxiosResponse } from 'axios';
import getConfig from '../../appSettings';
import { ExpenceModelUI } from '../uiModels/ExpenceModelUI';


import { TokenService } from './GetToken';
const service = new TokenService();

const token = service.getToken();
axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;

export  class ExpenceService {
    public async getExpenceData(): Promise< AxiosResponse<ExpenceModelUI[]>> { 
        return await axios.get(`${getConfig()}WeatherForecast/expencess`);
    }
}