import axios, { AxiosResponse } from 'axios';
import getConfig from '../../appSettings';
import { LoginModelUI } from '../../shared/uiModels/LoginModelUI';

interface Data {
    id: number;
    name: string;
}
const data = {
    name: 'John',
    email: 'john@example.com'
};



export interface ResponseData {
  
    name: string;
    email: string;
    token: string;
}

const token = 'your_jwt_token';
axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;


export  class LoginService {
    

    // public async getData(): Promise<AxiosResponse<Data[]>> {
    //     return  await axios.get(`${getConfig()}WeatherForecast`);
    // }

    public async postData(loginmodel:LoginModelUI): Promise< AxiosResponse<ResponseData>> { 
        return await axios.post(`${getConfig()}WeatherForecast/login`, loginmodel);
    }
}