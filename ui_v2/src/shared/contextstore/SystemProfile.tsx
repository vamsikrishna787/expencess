import React, { createContext, useCallback, useContext, useMemo, useReducer } from 'react';

export interface SystemInfo {
    loanId: number | null;
    propertyId: number | null;
};

interface SystemContextInterface {
    systemData: SystemInfo;
    updateSystemProfile(payload?: Partial<SystemInfo>): void;
    updatePartialSystemProfile(payload?: Partial<SystemInfo>): void;
    resetSystemProfile(): void;
};
export enum SystemProfileActions {
    UPDATE,
    RESET,
    PARTIAL_UPDATE,
}
const systemProfileInitialState: SystemInfo = {
    loanId: null,
    propertyId: null,
};


const systemProfileReducers = (state: SystemInfo, action: {
    type: SystemProfileActions;
    payload?: Partial<SystemInfo>;
}): SystemInfo => {
    switch (action.type) {
        case SystemProfileActions.UPDATE:
            return { ...action.payload } as SystemInfo;

        case SystemProfileActions.PARTIAL_UPDATE:
            return { ...state, ...action.payload } as SystemInfo;
        case SystemProfileActions.RESET:
            return systemProfileInitialState;
        default:
            throw new Error(`not supported`);
    }
};

const OriginationSystemContext = createContext<SystemContextInterface | undefined>(undefined);



type AuthComponentProps = {
    children: React.ReactNode;
};


export const AuthContextProvider: React.FC<AuthComponentProps> = ({ children }) => {
    const [systemProfile, systemProfileDispatch] = useReducer(systemProfileReducers, systemProfileInitialState);


    const resetSystemProfileHandler = useCallback(() => {
        systemProfileDispatch({ type: SystemProfileActions.RESET });
    }, []);

    const updateSystemProfileHandler = useCallback((payload: Partial<SystemInfo>) => {
        systemProfileDispatch({ type: SystemProfileActions.UPDATE, payload: payload });
    }, []);

    const updatePartialsystemProfileHandler = useCallback((payload: Partial<SystemInfo>) => {
        systemProfileDispatch({ type: SystemProfileActions.PARTIAL_UPDATE, payload: payload });
    }, []);


const context= useMemo<SystemContextInterface>(()=>({
    systemData:systemProfile,
    updateSystemProfile:updateSystemProfileHandler,
    resetSystemProfile:resetSystemProfileHandler,
    updatePartialSystemProfile:updatePartialsystemProfileHandler
}),[systemProfile,resetSystemProfileHandler,updateSystemProfileHandler,updatePartialsystemProfileHandler])


    return (
        <OriginationSystemContext.Provider value={context}>
            {children}
        </OriginationSystemContext.Provider>
    );
};

export function useOriginationConetxt(){
    const conetxt= useContext(OriginationSystemContext);
    if(conetxt==undefined)
    {
        throw new Error('something wrong');
    }
    return conetxt;
}