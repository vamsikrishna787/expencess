import React, { createContext, useContext, useState, useEffect } from 'react';


export interface AuthInfo {
    name?: string;
    isLoggedIn: boolean;
    email?: string;
    token?: string;
}


interface AuthContextInterface {
    authData: AuthInfo;
    Login: (data: AuthInfo) => void;
    LogOut: () => void;
}

export const AuthContext = React.createContext<AuthContextInterface>({
    authData: {
        name: "",
        isLoggedIn: false,
        email: "",
        token: ""
    },
    Login: () => { },
    LogOut: () => { },
});


type AuthComponentProps = {
    children: React.ReactNode;
};

// create a provider component for the context
export const AuthContextProvider: React.FC<AuthComponentProps> = ({ children }) => {

    const [authData, setData] = useState<AuthInfo>({
        name: undefined,
        isLoggedIn: false,
        email: undefined,
        token: undefined
    });

    useEffect(() => {
        const storedData = localStorage.getItem("person");
        if (storedData != null) {
            const myObject: AuthInfo = JSON.parse(storedData!);
            setData(myObject);
        }
    }, []);



    const LoginHandler = (data: AuthInfo) => {
        const serializedPerson = JSON.stringify(data);
        localStorage.setItem("person", serializedPerson);
        setData(data);
    }

    const LogOutHandler = () => {
        localStorage.removeItem('person');
        setData({ isLoggedIn: false, email: undefined, name: undefined, token: undefined });
    }

console.log(authData)

    const value = { authData, Login: LoginHandler, LogOut: LogOutHandler };
    return (
        <AuthContext.Provider value={value}>
            {children}
        </AuthContext.Provider>
    );
};