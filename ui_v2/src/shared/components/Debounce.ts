import { useCallback, useRef } from 'react';

type DebounceFunction = <T extends unknown[]>(func: (...args: T) => void, delay: number) => (...args: T) => void;

function useDebounce(): DebounceFunction {
  const timerRef = useRef<number | null>(null);

  const debounce = useCallback<DebounceFunction>((func, delay) => {
    return (...args) => {
      if (timerRef.current) {
        clearTimeout(timerRef.current);
      }
      timerRef.current = window.setTimeout(() => {
        func(...args);
      }, delay);
    };
  }, []);

  return debounce;
}

export default useDebounce;


//usage example
// import useDebounce from './useDebounce';

// function MyComponent() {
//   const [value, setValue] = useState('');
//   const debounce = useDebounce();

//   function handleChange(event: React.ChangeEvent<HTMLInputElement>) {
//     const { value } = event.target;
//     debounce(() => {
//       console.log('Value:', value);
//       // Make API call or perform search operation
//     }, 500)();
//   }

//   return (
//     <div>
//       <input type="text" value={value} onChange={handleChange} />
//     </div>
//   );
