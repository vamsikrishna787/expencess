import * as React from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import Box from '@mui/material/Box';



const Progress: React.FC = () => {


    return <>
        <Box sx={{ display: 'flex' }}>
            <CircularProgress />
        </Box>

    </>
}
export default Progress;


