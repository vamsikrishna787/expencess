import React, { ReactNode } from 'react';
import Error from './Error';
import ProgressSkelton from './ProgressSkelton';
import Progress from './Progress';

interface ErrorProps {
    error: boolean;
    isloading: boolean;
    children: ReactNode
}

const ErrorLoaderWrapper: React.FC<ErrorProps> = ({ error, isloading, children }) => {

    if (error) {
        return <Error />
    }
    else if (isloading) {
        return <ProgressSkelton/>
    }
    else
        return <>{children}</>
}
export default ErrorLoaderWrapper