import React, { useState } from 'react';
import { AgGridReact } from 'ag-grid-react';

interface GridProps {
  rowData: any[];
  columnDefs: any[];
}

const AgGrid: React.FC<GridProps> = ({ rowData, columnDefs }) => {
  const [gridApi, setGridApi] = useState(null);
  const [gridColumnApi, setGridColumnApi] = useState(null);

  const onGridReady = (params: any) => {
    setGridApi(params.api);
    setGridColumnApi(params.columnApi);
  };

  return (
    <div className="ag-theme-alpine" style={{ height: '500px', width: '600px' }}>
      <AgGridReact
        rowData={rowData}
        columnDefs={columnDefs}
        onGridReady={onGridReady}
      />
    </div>
  );
};

export default AgGrid;
