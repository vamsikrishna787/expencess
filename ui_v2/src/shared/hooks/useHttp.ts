import { useState, useEffect, useCallback } from 'react';
import axios, { AxiosResponse } from 'axios';





interface ApiCallState<T> {
  data: T | null;
  loading: boolean;
  error: Error | null;
}

export function useApiCall<T>(url: string): ApiCallState<T> {
  const [data, setData] = useState<T | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<Error | null>(null);

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await fetch(url);
        const json = await response.json();
        setData(json);
      } catch (errorr:any) {
        setError(errorr);
      } finally {
        setLoading(false);
      }
    }
    fetchData();
  }, [url]);

  return { data, loading, error };
}




interface HttpsOptions {
  url: string;
  method: 'GET' | 'POST' | 'PUT' | 'DELETE';
  headers?: any;
  data?: any;
}


export const useHttps = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [data,setData]=useState<any | null>(null);



  const sendRequest = (requestconfig: HttpsOptions) => {

    setIsLoading(true);

    const fetchData = async () => {
      try {
        const response: AxiosResponse<any> = await axios({
          ...requestconfig,
       
        });
        setData(response.data);
      } catch (error) {
          setError(true);
      } finally {
        setIsLoading(false);
      }
    };

    fetchData();
    setError(true);
  }
  return {
    isLoading,
    error,
    sendRequest
  }
}
