import React, {useContext,useEffect, useState} from 'react';

import {useSelector,useDispatch} from 'react-redux'
import {counterActions} from '../../shared/reduxstore/ReduxStore'
import { ExpenceService } from '../../shared/service/ExpenceService';
import { ExpenceModelUI } from '../../shared/uiModels/ExpenceModelUI';
import { AgGridReact } from "ag-grid-react";
import { ColDef, GridApi, GridReadyEvent } from "ag-grid-community";
import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';


const Expence:React.FC=()=>{
 //redux example
    // const counter=useSelector((state:any)=> state.counter);
    // const testva=useSelector((state:any)=> state.authenticator);
    // console.log(counter)
    // console.log(testva)
    // const dispatch= useDispatch();
    // const test=()=>{
    //     dispatch(counterActions.increment())
    //   }


    const service= new ExpenceService();

    const [expences,SetExpencess]= useState<ExpenceModelUI[]>([]);
    const [error,seterror]=useState(false);
    const[progress,setprogress]=useState(false);
      
    const fetchdata= async ()=>{
      setprogress(true);
      try {
        const response =await service.getExpenceData(); 
        SetExpencess(response.data);
      }
      catch (errorr) {
        seterror(true);
       }
       finally {
        setprogress(false);
       }
    }
    
    useEffect( ()=>{
      fetchdata();
      },[])

      function onGridReady(params: GridReadyEvent) {
        const api: GridApi = params.api;
        api.sizeColumnsToFit();
      }


      const DeleteRecord=(Id:number)=>{
        const newDatda = expences.filter((row) => row.expenceId !== Id);
        SetExpencess(newDatda);
      }

      const AddExpence=()=>{
const tempdat:ExpenceModelUI={
  expenceId:Math.random(), expenceAmont:20,expenceDate:new Date(),expenceName:"ehe",expenceType:2,includeBudget:true
};
SetExpencess(prev=>[...prev,tempdat]);
    }


      const columnDefs: ColDef[] = [
        { headerName: "ExpenceId", field: "expenceId" },
        { headerName: "ExpenceName", field: "expenceName" },
        { headerName: "ExpenceType", field: "expenceType" },
        { headerName: "ExpenceDate", field: "expenceDate" },
        { headerName: "IncludeBudget", field: "includeBudget" },
        { headerName: "ExpenceAmont", field: "expenceAmont" },
       
        {
          headerName: "Edit",
          cellRenderer: (props:any) => (
       
            <button onClick={()=>{console.log(props.data) }}>Edit</button>
          ),
        },

        {
          headerName: "Deletee",
          cellRenderer: (props:any) => (
       

           <button onClick={()=>{DeleteRecord(props.data.expenceId) }}>Delete</button>
          ),
        },

      ];

return<>

<div className="ag-theme-alpine" style={{ height: 400 }}>
  <button onClick={AddExpence}>Add Expence</button>
      <AgGridReact<ExpenceModelUI>
        columnDefs={columnDefs}
        rowData={expences}
        onGridReady={onGridReady}
      />
    </div>

</>
}
export default Expence