import React from 'react';
import { render, screen } from '@testing-library/react';

import Expence from './Expence';

test('renders learn react link', () => {
  render(<Expence />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
