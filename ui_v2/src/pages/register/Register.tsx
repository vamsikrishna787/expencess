import React, { useContext, useCallback, useState, useEffect } from 'react';
import styles from './Login.module.css';
import ErrorLoaderWrapper from "../../shared/components/ErrorLoaderWrapper";
import { useForm, Controller } from "react-hook-form";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import Avatar from '@mui/material/Avatar';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { LoginModelUI } from '../../shared/uiModels/LoginModelUI';
import { AuthContext } from '../../shared/contextstore/authContext';
import { RegisterService } from '../../shared/service/RegisterService';
import { NavLink, useNavigate } from 'react-router-dom';
import Error from '../../shared/components/Error';
import SimpleBackdrop from '../../shared/components/Backdrop';
import { RegisterModelUI } from '../../shared/uiModels/RegisterModelUI';


const initialValues: RegisterModelUI = {
    UserName: '',
    Password: '',
    Email:''
};

const Schema = yup.object().shape({
    UserName: yup.string().required('UserName is required'),
    Password: yup.string().required('Password is required'),
    Email: yup.string().email('Invalid email').required('Email is required'),
});
const service = new RegisterService();

const Register: React.FC = () => {

    const Navigate = useNavigate();

    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(false);
    const[iserror,setisError]= useState(false);
    const[isprocessing,setProcessing]= useState(false);

    //loading page first
    useEffect(() => {

    }, [])

    const { register, control, handleSubmit, formState: { errors } } = useForm<RegisterModelUI>({ resolver: yupResolver(Schema), defaultValues: initialValues });


    const onSubmit = async (formData: RegisterModelUI) => {
        setisError(false);
        setProcessing(true);
               try {
                  const response =await service.postData(formData); 
                   Navigate("/login")
                }
                catch (errorr) {
                    setisError(true);
                 }
                 finally {
                    setProcessing(false);
                 }
                }
    return (

        <ErrorLoaderWrapper error={error} isloading={isLoading}>
            <SimpleBackdrop show={isprocessing}/>
            <form onSubmit={handleSubmit(onSubmit)}>
             
                <Container component="main" maxWidth="xs">
                    <Box sx={{ marginTop: 8, display: 'flex', flexDirection: 'column', alignItems: 'center', }}>
                        <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}><LockOutlinedIcon /></Avatar>
                        <Typography component="h1" variant="h5">Sign Up</Typography>
                        {iserror&&<Error/> }
                        <TextField
                            label="Email"
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            {...register("Email")}
                            error={!!errors.Email}
                            helperText={errors.Email?.message}
                        />

                        <TextField
                            label="UserName"
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            {...register("UserName")}
                            error={!!errors.UserName}
                            helperText={errors.UserName?.message}
                        />

                        <TextField
                            label="Password"
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            {...register("Password")}
                            error={!!errors.Password}
                            helperText={errors.Password?.message}
                        />

                        <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}>Sign Up</Button>
                 
                        <Grid container>
              <Grid item xs>
              {/* <NavLink  to={'/password'}>
                  Forgot password?
                </NavLink> */}
              </Grid>
              <Grid item>
                <NavLink  to={'/login'}>
                  {"Have an account? Sign In"}
                </NavLink>
              </Grid>
            </Grid>
   
                    </Box>
                </Container>
            </form>
        </ErrorLoaderWrapper>

    )
}
export default Register