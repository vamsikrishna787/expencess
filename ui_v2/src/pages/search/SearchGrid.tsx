import React,{useState} from "react"
import { Car } from "./Search"
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';
import { Link } from 'react-router-dom';
import { ColDef, GridApi, GridReadyEvent } from "ag-grid-community";


interface ChildProps{
    dat:Car[]
}

interface EmailLinkRendererProps {
    value: string;
  }
  
  function EmailLinkRenderer(props: EmailLinkRendererProps) {
    return <Link to={props.value} target='_blank'>{props.value}</Link>;
  }

const SearchGrid:React.FC<ChildProps>=({dat})=>{



const columnDefs: ColDef[] = [
    { headerName: "make", field: "make" },
    { headerName: "model", field: "model" },
    {
      headerName: "make",
      field: "make",
      cellRendererFramework: EmailLinkRenderer,
    },
  ];

      function onGridReady(params: GridReadyEvent) {
        const api: GridApi = params.api;
        api.sizeColumnsToFit();
      }
    

    console.log(dat)
return<>
 <div className="ag-theme-alpine" style={{ height: 400 }}>
     
      <AgGridReact<Car>
        columnDefs={columnDefs}
        rowData={dat}
        onGridReady={onGridReady}
      />

       </div>
</>
}
export default SearchGrid;