import React, { useState, useCallback } from "react"
import SearchGrid from "./SearchGrid";
import SearchHeader from "./SearchHeader";



export interface Car {
    make: string,
    model: string,
    price: number
}

const initaldata: Car[] = [
    { make: "Toyota", model: "Celica", price: 35000 },
    { make: "Toyota", model: "colrola", price: 35000 },
    { make: "Ford", model: "Mondeo", price: 32000 },
    { make: "Porsche", model: "Boxster", price: 72000 }
]
const Search: React.FC = () => {

    const [rowData,setrowdata] = useState<Car[]>(initaldata);


const filterdata= useCallback( (data:string)=>{
const result= initaldata.filter(x=>x.make==data);
setrowdata(result);
},[])



    return <>
        <SearchHeader onData={filterdata} />
        <SearchGrid dat={rowData} />
    </>
}
export default Search