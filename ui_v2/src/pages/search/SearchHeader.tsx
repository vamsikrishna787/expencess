
import React,{useRef} from "react"
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
 interface ChildProps {
    onData: (data: string) => void;
  }

  
const SearchHeader:React.FC<ChildProps>=({onData})=>{


    const inputRef = useRef<HTMLInputElement>(null);

const senddata=()=>{
    if (inputRef.current) {
        onData(inputRef.current.value);
        inputRef.current.value = "";

       }
}

return<>


<TextField inputRef={inputRef} label="Input" />

<Button variant="text" onClick={senddata}>Search</Button>
</>
}
export default  React.memo(SearchHeader);