import React, { useState,useEffect } from 'react'
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';

interface AddExpenceProps{
    // username:string
    onClick: () => void;
    
}

const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ username: 'user', password: 'pass' })
  };
  

const HomeGrid:React.FC<AddExpenceProps>=({onClick})=>{
    const [isporgress,setprogress]=useState<boolean>(false)
    const [data,setRowData]=useState<any>(undefined);
   
    const [columnDefs] = useState([
        { field: 'make' },
        { field: 'model' },
        { field: 'price' }
    ])

//post
    // fetch('https://example.com/api/login', requestOptions)
    // .then(response => response.json())
    // .then(data => console.log(data))
    // .catch(error => console.error('Error:', error));


//axios get
    // axios.get('https://example.com/api/data')
    // .then(response => setData(response.data))
    // .catch(error => console.error('Error fetching data:', error));


    
//with headeer & without await
//     const options = {
//         headers: {
//           'Authorization': 'Bearer my-token',
//           'Content-Type': 'application/json'
//         }
//       };
//       axios.get('https://example.com/api/data', options)
//   .then(response => console.log(response.data))
//   .catch(error => console.error('Error fetching data:', error));


//await
// try {
//     const response = await axios.get(`${getConfig()}WeatherForecast`);
//     setData(response.data);
//   } catch (error) {
//     console.error('Error fetching data:', error);
//   }

    useEffect(() => {
        setprogress(true);
        fetch('https://www.ag-grid.com/example-assets/row-data.json')
        .then(result => result.json())
        .then(rowData => setRowData(rowData))
        .catch(error => console.error('Error:', error))
        .finally(()=>{  setprogress(false)})
        
     }, []);

     console.log(data)


   return<>
    <div className="ag-theme-alpine" style={{height: 400, width: 600}}>
           <AgGridReact
               rowData={data}
               columnDefs={columnDefs}>
           </AgGridReact>
       </div>
   </>
} 
export default React.memo(HomeGrid)