import React from "react";
import { AgGridReact } from "ag-grid-react";
import { ColDef, GridApi, GridReadyEvent } from "ag-grid-community";
import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';

interface RowData {
  id: number;
  name: string;
  email: string;
}

export interface Car {
    make: string,
    model: string,
    price: number
}
interface DeleteButtonRendererProps {
  onDelete: () => void;
}

function DeleteButtonRenderer(props: DeleteButtonRendererProps) {
  return (
    <button className="delete-button" onClick={props.onDelete}>
      <i className="fa fa-trash"></i>Delete
    </button>
  );
}

function HomeEditGrid() {
  const [rowData, setRowData] = React.useState<RowData[]>([
    { id: 1, name: "John", email: "john@example.com" },
    { id: 2, name: "Jane", email: "jane@example.com" },
  ]);

  const columnDefs: ColDef[] = [
    { headerName: "ID", field: "id" },
    { headerName: "Name", field: "name" },
    {
      headerName: "Actions",
      cellRenderer: (props:any) => (
        <DeleteButtonRenderer
          onDelete={() => {
            const data= props.data as RowData
            const newData = rowData.filter((row) => row.id !== data.id);
            setRowData(newData);
          }}
        />
      ),
    },
  ];

  function onGridReady(params: GridReadyEvent) {
    const api: GridApi = params.api;
    api.sizeColumnsToFit();
  }

  return (
    <div className="ag-theme-alpine" style={{ height: 400 }}>
      <AgGridReact<RowData>
        columnDefs={columnDefs}
        rowData={rowData}
        onGridReady={onGridReady}
      />
    </div>
  );
}

export default HomeEditGrid;
