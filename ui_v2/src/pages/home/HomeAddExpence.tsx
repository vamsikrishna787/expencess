import React, { useState,useEffect } from 'react'
import { AgGridReact } from 'ag-grid-react';
import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';
interface AddExpenceProps{
    // username:string
    list:number[]| undefined;
}

const HomeAddExpence:React.FC<AddExpenceProps>=(expenceprops)=>{

    const [isporgress,setprogress]=useState<boolean>(false)
    const [data,setRowData]=useState<any>(undefined);
   
    const [columnDefs] = useState([
        { field: 'make' },
        { field: 'model' },
        { field: 'price' }
    ])

    useEffect(() => {
        setprogress(true);
       
        const fetchData = async () => {
            try {
              const response = await fetch('https://www.ag-grid.com/example-assets/row-data.json');
              const data = await response.json();
              setRowData(data);
            } catch (error) {
              console.error('Error fetching row data:', error);
            }
            finally {
                setprogress(false);
              }
            
          };
          fetchData();


        
     }, []);

     console.log(data)


   return<>
    <div className="ag-theme-alpine" style={{height: 400, width: 600}}>
           <AgGridReact
               rowData={data}
               columnDefs={columnDefs}>
           </AgGridReact>
       </div>
   </>
}
export default React.memo(HomeAddExpence)