import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import HomeGrid from './HomeGrid';
import HomeAddExpence from './HomeAddExpence';
import HomeEditGrid from './HomeEditGrid';




// const Item = styled(Paper)(({ theme }) => ({
//   backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
//   ...theme.typography.body2,
//   padding: theme.spacing(1),
//   textAlign: 'center',
//   color: theme.palette.text.secondary,
// }));

 const Home:React.FC=()=>{
  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container spacing={2}>
       
        <Grid item xs={6}>
          <Paper>
            <HomeGrid onClick={()=>{}}/>
          </Paper>
        </Grid>

        <Grid item xs={6} style={{padding:'10px'}}>
          <Paper>
          <HomeAddExpence list={[1,2]}/>
          </Paper>
        </Grid>

        <Grid item xs={6} style={{padding:'10px'}}>
          <Paper>
          <HomeEditGrid />
          </Paper>
        </Grid>

     
      </Grid>
    </Box>
  );
}
export default Home;