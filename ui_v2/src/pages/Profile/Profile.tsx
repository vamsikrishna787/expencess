import React from 'react';
import { useForm, useFieldArray, Controller } from 'react-hook-form';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
 import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import Delete from '@mui/material/Icon';
import { getValue } from '@testing-library/user-event/dist/utils';

import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';




interface ListItem {
    id?:number
    cardName: string;
    cardNumber: string;
  }
  
  interface ListData {
    items: ListItem[];
    fullName:string;
  }

const schema = yup.object().shape({
  items: yup.array().of(
    yup.object().shape({
      cardName: yup.string().required('CardName is required'),
      cardNumber:  yup.string().required('CardName is required'),
    })
  ),
  fullName: yup.string().required('FullName name is required'),
});




const Profile: React.FC = () => {






 
  const { control,register, handleSubmit,formState: { errors }  } = useForm<ListData>({
    
    resolver: yupResolver(schema),
    defaultValues: {items: [{ cardName: '', cardNumber: '' ,id:Math.random()}],
    fullName:''
    }
  });

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'items'
  });

  const onSubmit = (data: ListData) => {
    console.log(data);
  };

  const addItem = () => {
    append({ cardName:'',cardNumber:'',id:Math.random()});
  };

  const removeItem = (index: number) => {
    remove(index);
  };

  
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
    
     <TextField
        label="FullName"
        variant="outlined"
        margin="normal"
        fullWidth
        {...register("fullName")}
        error={!!errors.fullName}
        helperText={errors.fullName?.message}
      />

     
      <Grid container spacing={2}>
        <Grid item>
          <Button variant="contained" color="primary" onClick={addItem}>Add Item</Button>
        </Grid>
        <Grid item xs={12}>
          {fields.map((field, index) => (
           
           <Grid container spacing={2} key={field.id}>

              <Grid item xs={4}>
                <Controller
                  control={control}
                
                  name={`items.${index}.cardName` as const}
                  defaultValue={field.cardName}
                  render={({ field }) => (
                    <TextField {...field} label="CardName" fullWidth error={!!errors.items?.[index]?.cardName}  helperText={errors.items?.[index]?.cardName?.message}  />
                  )}
                />
              </Grid>

              <Grid item xs={4}>
                <Controller
                  control={control}
                  name={`items.${index}.cardNumber` as const}
                  defaultValue={field.cardNumber}
                  render={({ field }) => (
                    <TextField {...field} label="CardNumber" fullWidth error={!!errors.items?.[index]?.cardNumber}  helperText={errors.items?.[index]?.cardNumber?.message}  />
                  )}
                />
              </Grid>

              <Grid item xs={4}>
              <Button variant="contained" color="primary"   onClick={() => removeItem(index)}  >Delete Item</Button>
              
          
              </Grid>
            </Grid>
          ))}
        </Grid>
        <Grid item xs={12}>
          <Button type="submit" variant="contained" color="primary">Submit</Button>
        </Grid>
      </Grid>


    </form>
  );
};

export default Profile;
