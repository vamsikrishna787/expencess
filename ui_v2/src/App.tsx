import { Suspense } from 'react';
import { RouterProvider } from 'react-router-dom';
import Progress from './shared/components/Progress';
import RooterMaster from './shared/router/RooterMaster';
import { AuthContextProvider } from '../src/shared/contextstore/authContext';
import { Provider } from 'react-redux';
import store from '../src/shared/reduxstore/ReduxStore'

function App() {
  return <Provider store={store}>
    <AuthContextProvider>
      <Suspense fallback={<Progress />}> <RooterMaster /></Suspense>
    </AuthContextProvider>
  </Provider>
}
export default App;