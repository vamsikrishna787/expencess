﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;


namespace API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {

        string secretKey = "YourSecretKeytjtr4jutrurturtutrusrdghdvgdfhdhferyy";

        // Define your user's login credentials
        string username = "exampleUser";
        string password = "examplePassword";



        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        //        [HttpGet]
        //        public IEnumerable<WeatherForecast> Get()
        //        {


        //            var claims = new[]
        //{
        //    new Claim(ClaimTypes.Name, username)
        //};

        //            // Generate signing key from secret key
        //            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));

        //            // Create credentials and specify the algorithm
        //            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

        //            // Create token descriptor
        //            var tokenDescriptor = new SecurityTokenDescriptor
        //            {
        //                Subject = new ClaimsIdentity(claims),
        //                Expires = DateTime.UtcNow.AddDays(1),
        //                SigningCredentials = credentials
        //            };

        //            // Create token handler
        //            var tokenHandler = new JwtSecurityTokenHandler();

        //            // Generate token using token handler
        //            var token = tokenHandler.CreateToken(tokenDescriptor);

        //            // Serialize token to string
        //            var tokenString = tokenHandler.WriteToken(token);


        //            var rng = new Random();
        //            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
        //            {
        //                Date = DateTime.Now.AddDays(index),
        //                TemperatureC = rng.Next(-20, 55),
        //                Summary = Summaries[rng.Next(Summaries.Length)]
        //            })
        //            .ToArray();
        //        }

        public class LoginModel
        {
            public string UserName { get; set; }
            public string Password { get; set; }
        }


        public class RegisterModel
        {
            public string UserName { get; set; }
            public string Password { get; set; }
            public string Email { get; set; }
        }


        public class ResponseModel
        {

            public string name { get; set; }
            public string email { get; set; }
            public string token { get; set; }
        }



        public class Expences
        {
            public int ExpenceId { get; set; }
            public string ExpenceName { get; set; }
            public int ExpenceType { get; set; }
            public DateTime ExpenceDate { get; set; }
            public bool IncludeBudget { get; set; }
            public int ExpenceAmont { get; set; }
            public int[] Categories { get; set; }

        }

        [HttpPost("login")]
        public async Task<IActionResult> LoginAsync([FromBody] LoginModel login)
        {
            if (login.UserName == "test" && login.Password == "test")
            {
                ResponseModel responseModel = new ResponseModel();
                responseModel.name = "vamsi";
                responseModel.email = "vamsi@gmail";
                responseModel.token = "vamwerferferfsi";

                // Successful login
                return Ok(responseModel);
            }
            else
            {
                // Failed login
                return Unauthorized("Incorrect username or password.");
            }
        }




        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync([FromBody] RegisterModel model)
        {
            // Successful login
            return Ok();
        }



        [HttpGet("expencess")]
        public async Task<IActionResult> GetExpencessAsync()
        {
            List<Expences> expences = new List<Expences>();
           
            int[] myArray = { 1, 2, 3, 4, 5 };

            Expences expences1 = new Expences();
            expences1.ExpenceId = 1;
            expences1.ExpenceName = "test";
            expences1.ExpenceDate = DateTime.Now;
            expences1.IncludeBudget = true;
            expences1.ExpenceType = 1;
            expences1.ExpenceAmont = 22;
            expences1.Categories = myArray;

            Expences expences2 = new Expences();
            expences2.ExpenceId = 2;
            expences2.ExpenceName = "test2";
            expences2.ExpenceDate = DateTime.Now;
            expences2.IncludeBudget = true;
            expences2.ExpenceType = 1;
            expences2.ExpenceAmont = 22;
            expences2.Categories = myArray;

            expences.Add(expences1);
            expences.Add(expences2);

            return Ok(expences);
        }



        }
    }



