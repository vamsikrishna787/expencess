import React,{lazy,Suspense} from 'react';
import logo from './logo.svg';
import './App.css';
import SignIn from './pages/Login/SignIn';
import SignUp from './pages/Registartion/SignUp';
// import Concepts from './pages/Concepts/Concepts'

import Layout from './pages/Shared/Layouts/MainNavigation';
import Expence from './pages/Expences/Expence';
import ExpenceDetail from './pages/ExpencesDetails/ExpenceDetail';
import NewExpence from './pages/Expences/NewExpence';
import EditExpence from './pages/Expences/EditExpence';
import RootLayout from './pages/Shared/Layouts/RootLayout';
import Error from './pages/Shared/Pages/Error';
import axios from 'axios';
import {createBrowserRouter,createRoutesFromElements,Route,RouterProvider,redirect} from 'react-router-dom';

import Sample from './pages/Sample/Sampl';
const Concepts =lazy(()=>import('./pages/Concepts/Concepts'));
// const requestAPI = async () => {
//   try {
//     const res = await axios.get(`API_URL`, {
//       headers: {},
//       params: {}
//     });
//   } catch (err) {
//     console.log(err);
//   }
// };

const sample=()=>{
 // return redirect('/');
 return false;
}


const routes=createBrowserRouter([
    { path:'/',element:<RootLayout/>,errorElement:<Error/>,children:[
      {index:true,element:<SignIn/>},
      {path:'signup',element:<SignUp/>,loader:sample},
      {path:'concept',
      element: <Suspense fallback={<p>Loading..</p>}><Concepts/></Suspense>,
      loader:(meta)=> import('./pages/Concepts/Concepts').then((module)=>module.default) },
      {path:'expence',element:<Expence/>},
      {path:'expence/:expenceId',element:<ExpenceDetail/>},
      {path:'expence/new',element:<NewExpence/>},
      {path:'expence/:expenceId/edit',element:<EditExpence/>}
      ]
    }

]);

//Alternate Way
// const reactroutesfromelemnt=createRoutesFromElements(
//   <Route>
//    <Route path="/" element={<SignIn/>}/>
//    <Route path="/signup" element={<SignUp/>}/>
//   </Route>
// );
// const routes=createBrowserRouter(reactroutesfromelemnt);


function App() {
  
  return  <Suspense fallback={<div>Loading...</div>}>
              <RouterProvider router={routes}/>
  </Suspense>

}

export default App;
