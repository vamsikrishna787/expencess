import React, { useState, useCallback } from 'react';

function Sample(): JSX.Element {
  const [value, setValue] = useState('');

  const debounce = useCallback(
    <T extends unknown[]>(func: (...args: T) => void, delay: number) => {
      let timerId: ReturnType<typeof setTimeout> | null = null;
      return function debounced(...args: T) {
        if (timerId) {
          clearTimeout(timerId);
        }
        timerId = setTimeout(() => {
          func(...args);
        }, delay);
      };
    },
    []
  );

  const handleInputChange = useCallback(
    debounce((newValue: string) => {
      console.log(newValue);
    }, 500),
    [debounce]
  );

  return (
    <input
      type="text"
      value={value}
      onChange={(event) => {
        setValue(event.target.value);
        handleInputChange(event.target.value);
      }}
    />
  );
}
export default Sample;