import React, { useContext, useEffect } from 'react';
import { useForm, Controller } from "react-hook-form";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";



import Avatar from '@mui/material/Avatar';

import CssBaseline from '@mui/material/CssBaseline';

import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';


import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import FormHelperText from '@mui/material/FormHelperText';

import AuthContext from    '../Shared/ContextStore/authContext';
import { MyContext } from '../Shared/ContextStore/myContext';
import {LoginService} from '../Shared/Service/LoginService';



import{CounterContext} from  '../Shared/ContextStore/authenticationContext';
import{useInput} from '../Shared/Hooks/useInput';

import{useHttp1} from '../Shared/Hooks/useHttp1';
interface LoginFormData {
  email: string;
  password: string;
  selectedOption: number | null;
}


interface DropdownOption {
  id: number;
  label: string;
}


class Person {
  constructor(public name: string, public age: number) {}
}



//alias types
let gh:number|string;

//genric exmple
function testj<T>(array:T[])
{

}
const options: DropdownOption[] = [

  { id: 1, label: 'Option 1' },
  { id: 2, label: 'Option 2' },
  { id: 3, label: 'Option 3' },
];

const initialValues: LoginFormData = {
  email: '',
  password: '',
  selectedOption: null
};

const LoginSchema = yup.object().shape({
  email: yup.string().email('Invalid email').required('Email is required'),
  password: yup.string().required('Password is required'),
  selectedOption: yup.number().required('Please select an option'),
});


const service = new LoginService();



const SignIn: React.FC = () => {

  const { register, control, handleSubmit, formState: { errors } } = useForm<LoginFormData>({ resolver: yupResolver(LoginSchema), defaultValues: initialValues, });

  const { datas, setData } = useContext(MyContext);


  const {
    value: enteredName,
    isValid: enteredNameIsValid,
    hasError: nameInputHasError,
    valueChangeHandler: nameChangedHandler,
    inputBlurHandler: nameBlurHandler,
    reset: resetNameInput,
    sample:test
  } = useInput("skls","qwfqwf");

  const { isLoading, error, sendRequest: sendTaskRequest } = useHttp1();

  const onSubmit = (data: any) => {
    console.log(data);
    increment();
     setData({ ...datas, name: "Alice" });
     increment();
    // service.getData();


    sendTaskRequest(
      {
        url: 'https://react-http-6b4a6.firebaseio.com/tasks.json',
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: { text: "" },
      },
    ""
    );
    
  };

  const theme = createTheme();

  // const ctx = useContext(AuthContext);

  const { count,increment, decrement } = useContext(CounterContext);


  // useEffect(() => {
    
  // },[]);



console.log(count)

  // console.log(data)
  


 
 

  // Create an instance of the class
  const person = new Person("John", 30);
  // Serialize the object to JSON format
  const serializedPerson = JSON.stringify(person);
  localStorage.setItem("person", serializedPerson);


  const serializedPersond = localStorage.getItem("person");
  const persondes = JSON.parse(serializedPerson, (key, value) => {
    if (key === "") return value;
    return new Person(value.name, value.age);
  });




  localStorage.setItem('isLoggedIn', '1');
  const storedUserLoggedInInformation = localStorage.getItem('isLoggedIn');
  localStorage.removeItem('isLoggedIn')
  
  return <>

<AuthContext.Consumer>
{({ isloggedin }) => <h1>Current Theme: {isloggedin}</h1>}
</AuthContext.Consumer>



<form onSubmit={handleSubmit(onSubmit)}>

<ThemeProvider theme={theme}>
  <Container component="main" maxWidth="xs">
    <CssBaseline />
    <Box
      sx={{
        marginTop: 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
        <LockOutlinedIcon />
      </Avatar>
      <Typography component="h1" variant="h5">
        Sign in {datas.name}{count}
      </Typography>

      <TextField
        label="email"
        variant="outlined"
        margin="normal"
        fullWidth
        {...register("email")}
        error={!!errors.email}
        helperText={errors.email?.message}
        onChange={() => { console.log("sample") }}
      />


      <TextField
        label="password"
        variant="outlined"
        margin="normal"
        fullWidth
        {...register("password")}
        error={!!errors.password}
        helperText={errors.password?.message}
        onBlur={() => { console.log("sample") }}
      />


      <FormControl variant="outlined" fullWidth error={!!errors.selectedOption}>
        <InputLabel id="dropdown-label">Select an option</InputLabel>
        <Controller
          name="selectedOption"
          control={control}

          render={({ field }) => (
            <>
              <Select
                {...field}
                labelId="dropdown-label"
                id="dropdown"
                label="Select an option"
              >
                {options.map(option => (
                  <MenuItem key={option.id} value={option.id}>
                    {option.label}
                  </MenuItem>
                ))}
              </Select>
              <FormHelperText>{errors.selectedOption?.message}</FormHelperText>
            </>
          )}
        />
      </FormControl>




      <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}
      >
        Sign In
      </Button>
    </Box>
  </Container>
</ThemeProvider>


</form>
  </>
};

export default SignIn;
