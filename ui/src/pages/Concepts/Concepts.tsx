import React, { useContext,useRef, useEffect,lazy,Suspense } from 'react';
import Button from '@mui/material/Button';
import Portal from '../Shared/Components/Portal';
// import List from './RenderList';
import{items as ItemProp } from './RenderList';

const List =lazy(()=>import('./RenderList'))


const styles = {
    container: {
      display: "flex",
      justifyContent: "center"
    },
    list: {
      display: "flex",
      listStyle: "none",
      padding: 0
    },
    listItem: {
      marginRight: "10px"
    }
  };


const Concepts: React.FC = (props) => {
    const [open, setOpen] = React.useState<boolean>(false);

    console.log("hd")

    const inputRef = useRef<HTMLInputElement>(null);

    const handleClick = () => {
      if (inputRef.current) {
       console.log(inputRef);
      }
    };


const handleClose=()=>{
    setOpen(false);
}

return<>
<h1>Hallo</h1>
  <input type="text" ref={inputRef} className={`form-control ${open? 'invalid':''}`} style={{backgroundColor:'red'}} />
      <button onClick={handleClick}>Focus Input</button>

{open&&(<Portal showflag={open} onConfirm={handleClose} />)}


<div style={styles.container}>
          <ul style={styles.list}>
            <li style={styles.listItem}>Item 1</li>
            <li style={styles.listItem}>Item 2</li>
            <li style={styles.listItem}>Item 3</li>
            <li style={styles.listItem}>Item 4</li>
            <li style={styles.listItem}>Item 4</li>
          </ul>
        </div>

<Button onClick={()=>{setOpen((value)=>true)}}>Test Props</Button>
    <Suspense fallback={<p>Loading...</p>}><List items={ItemProp}/></Suspense>  
</>
}
export default Concepts;
