import {render,screen} from '@testing-library/react';
import Concepts from './Concepts' 
import userEvent from '@testing-library/user-event'


// test suites
describe('Greeting Component',()=>{

    test("dd",()=>{
        //Arrange
        render(<Concepts />);
        
        //Act
        
        //Assert
        const helloworld= screen.getByText('Hallo')
        expect(helloworld).toBeInTheDocument();
        
        })



        //fail becuase  multiple buttons
        test("interaction event",()=>{
            //Arrange
            render(<Concepts />);
            
            //Act
           
            const buttonelememnt= screen. getByRole('button');
            userEvent.click(buttonelememnt);
            
            //Assert
            const helloworld= screen.queryByText('Hallo',{exact:false})
            expect(helloworld).toBeInTheDocument();
            
            })
    

})


