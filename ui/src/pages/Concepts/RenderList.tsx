import React, { useState,useEffect } from "react";

interface ListItem {
  id: number;
  name: string;
  price:number;
}

interface ListProps {
  items: ListItem[];
}


export const items: ListItem[] = [



    {
      id: 1,
      name: "Item 1",
      price: 9.99,
    },
    {
      id: 2,
      name: "Item 2",
      price: 14.99,
    },
    {
      id: 3,
      name: "Item 3",
      price: 19.99,
    },
  ];

const List: React.FC<ListProps> = ({ items }) => {
  const [selectedItem, setSelectedItem] = useState<ListItem | null>(null);
  useEffect(()=>{
    setTimeout(() => {
      console.log("Delayed for 1 second.");
    }, 9999999999);
  },[])

  const handleItemClick = (item: ListItem) => {
    setSelectedItem(item);
  };

  const doubledItems = items.map((item) => {
    return {
      ...item,
      price: item.price * 2,
    };
  });
  
//   const filter=(...args)=>{
//     return args  
//     }
//     console.log(filter(1,2,3))
    
//     const ui=[1,2,3]
//     const gh= [...ui,4]
//     console.log(gh)

  return (
    <div style={{height:'20px'}}>
      <ul>
        {items.map((item) => (
          <li key={item.id} onClick={() => handleItemClick(item)}>
            {item.name}
          </li>
        ))}
      </ul>
      {selectedItem && (
        <p>You selected: {selectedItem.name}</p>
      )}
    </div>
  );
};

export default React.memo(List);
