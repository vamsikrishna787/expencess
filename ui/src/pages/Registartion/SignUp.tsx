import React from 'react';
import { useForm, useFieldArray, Controller } from 'react-hook-form';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
 import Grid from '@mui/material/Grid';
import IconButton from '@mui/material/IconButton';
import Delete from '@mui/material/Icon';
import { getValue } from '@testing-library/user-event/dist/utils';

import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import AgGrid  from '../Shared/Components/AgGrid';

import {useSelector,useDispatch} from 'react-redux'
import {counterActions} from '../Shared/ReduxStore/ReduxStore'

const rowData = [
  { make: 'Toyota', model: 'Celica', price: 35000 },
  { make: 'Ford', model: 'Mondeo', price: 32000 },
  { make: 'Porsche', model: 'Boxter', price: 72000 },
];

const columnDefs = [
  { headerName: 'Make', field: 'make' },
  { headerName: 'Model', field: 'model' },
  { headerName: 'Price', field: 'price' },
];

// const schema = yup.object().shape({
//   street: yup.string().required('Name is required'),
//   name: yup.number().typeError('Age must be a number').positive('Age must be positive').required('Age is required'),
// });


const schema = yup.object().shape({
  // itemName: yup.string().required('Item name is required'),
  items: yup.array().of(
    yup.object().shape({
      name: yup.string().required('Name is required'),
      age: yup.number().typeError('Age must be a number').positive('Age must be positive').required('Age is required'),
    })
  ),
  street: yup.string().required('First name is required'),
  // lastName: yup.string().required('Last name is required'),
});

interface ListItem {
  id: number;
  name: string;
  age: number;
}

interface ListData {
  items: ListItem[];
  street:string;
}

const EditableList: React.FC = () => {


 const counter=useSelector((state:any)=> state.counter.value);
 const testva=useSelector((state:any)=> state.authenticator.loggedin);



 const dispatch= useDispatch();
const test=()=>{
  dispatch(counterActions.increment())
  dispatch(counterActions.incrementByAmount(6))
}


 
  const { control,register, handleSubmit,formState: { errors }  } = useForm<ListData>({
    resolver: yupResolver(schema),
    defaultValues: {
      items: [{ id: Date.now(), name: '', age: 0 }],
      street:""
    }
  });

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'items'
  });

  const onSubmit = (data: ListData) => {
    console.log(data);
  };

  const addItem = () => {
    append({ id: Date.now(), name: '', age: 0 });
  };

  const removeItem = (index: number) => {
    remove(index);
  };

  
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
     <Button onClick={test}>hj{counter}</Button>
     

     <TextField
        label="street"
        variant="outlined"
        margin="normal"
        fullWidth
        {...register("street")}
        error={!!errors.street}
        helperText={errors.street?.message}
  
      />

     
      <Grid container spacing={2}>
        <Grid item>
          <Button variant="contained" color="primary" onClick={addItem}>Add Item</Button>
        </Grid>
        <Grid item xs={12}>
          {fields.map((field, index) => (
            <Grid container spacing={2} key={field.id}>
              <Grid item xs={4}>
                <Controller
                  control={control}
                
                  name={`items.${index}.name` as const}
                  defaultValue={field.name}
                  render={({ field }) => (
                    <TextField {...field} label="Name" fullWidth error={!!errors.items?.[index]?.name}  helperText={errors.items?.[index]?.name?.message}  />
                  )}
                />
              </Grid>
              <Grid item xs={4}>
                <Controller
                  control={control}
                  name={`items.${index}.age` as const}
                  defaultValue={field.age}
                  render={({ field }) => (
                    <TextField {...field} label="Age" type="number" fullWidth error={!!errors.items?.[index]?.age}  helperText={errors.items?.[index]?.age?.message}  />
                  )}
                />
              </Grid>
              <Grid item xs={4}>
              
              <Button variant="contained" color="primary"   onClick={() => removeItem(index)}  >Delete Item</Button>
              
          
              </Grid>
            </Grid>
          ))}
        </Grid>
        <Grid item xs={12}>
          <Button type="submit" variant="contained" color="primary">Submit</Button>
        </Grid>
      </Grid>

      <div className="App">
      <AgGrid rowData={rowData} columnDefs={columnDefs} />
    </div>
    </form>
  );
};

export default EditableList;
