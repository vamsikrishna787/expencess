
import{createSlice,configureStore,PayloadAction} from '@reduxjs/toolkit'

interface CounterState {
    value: number;
    state:boolean
  }

  interface authy{
    loggedin:boolean
  }
  
  const initialStatetest: CounterState = {
    value: 0,
    state:false
  };

  const initalauthstate:authy={
    loggedin:true
  }



const counterslice = createSlice({
    name:'counter',
     initialState:initialStatetest,
    reducers:{
        increment(state){
            state.value++;
        },
        decrement(state){
            state.value--;
        },
        incrementByAmount: (state, action: PayloadAction<number>) => {
            state.value += action.payload;
          },
          toggle(state){
            // !state.state
          }
    }
})



const authslice = createSlice({
    name:'authnticator',
     initialState: initalauthstate,
    reducers:{
        loogin(state){
            state.loggedin=true;
        },
        loogout(state){
            // !state.loggedin
            state.loggedin=false;
        },
       
    }
})




// const counterReducer=(state={initalstate},action:any)=>{
//     if(action.type==="increment")
//     {
//         return {
//             counter:state.counter+1,
//             toggle:!state.toggle
//         }
//     }
//     if(action.type==="decrement")
//     {
//         return {
//             counter:state.counter-1+action.amount,
//             toggle:!state.toggle
//         }
//     }
//     return state;
// }


const store= configureStore({
    // reducer:counterslice.reducer
    reducer:{counter:counterslice.reducer,authenticator:authslice.reducer}
});


export const counterActions = counterslice.actions;
export const authActions=counterslice.actions;


export default store;