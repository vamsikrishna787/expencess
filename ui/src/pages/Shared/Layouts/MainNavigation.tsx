import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import GlobalStyles from '@mui/material/GlobalStyles';
import { NavLink,useNavigate } from 'react-router-dom';
import classes from './MainStyle.module.css';


function MainNavigation() {

const navigate=useNavigate();

const handleclick=()=>{
  navigate("/");
}

  return (
    <React.Fragment>
      <GlobalStyles styles={{ ul: { margin: 0, padding: 0, listStyle: 'none' } }} />
      <CssBaseline />
      <AppBar
        position="static"
        color="primary"
        elevation={0}
        sx={{ borderBottom: (theme) => `1px solid ${theme.palette.divider}` }}
      >
        <Toolbar sx={{ flexWrap: 'wrap' }}>
          <Typography variant="h6" color="inherit" noWrap sx={{ flexGrow: 1 }}>
            Expences
          </Typography>
          <nav>
           
           <NavLink to="/" className={({isActive})=> isActive ? classes.active:undefined} end>Home</NavLink>
           <NavLink to="/signup"  className={({isActive})=> isActive ? classes.active:undefined}>SignUp</NavLink>
          

          </nav>
          <Button onClick={handleclick} variant="outlined" sx={{ my: 1, mx: 1.5 }}>
            Login
          </Button>
        </Toolbar>
      </AppBar>
      {/* Hero unit */}
     
      {/* End footer */}
    </React.Fragment>
  );
}

export default function Pricing() {
  return <MainNavigation />;
}