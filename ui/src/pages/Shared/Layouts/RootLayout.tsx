
import React,{useState} from 'react';
import { Outlet } from "react-router-dom";
import MainNavigation from './MainNavigation'
import AuthContext from    '../ContextStore/authContext';
import { MyData } from '../ContextStore/myContext';
import { MyContext } from '../ContextStore/myContext';
import {CounterProvider} from '../ContextStore/authenticationContext'
import { Provider } from 'react-redux';
import store from '../ReduxStore/ReduxStore'

const RootLayout: React.FC = (props) => {

    const [datas, setData] = useState<MyData>({
        name: "John",
        age: 30,
        email: "john@example.com",
      });



    return <>
<Provider store={store}>
<CounterProvider>
<MyContext.Provider value={{ datas, setData }}>
    <AuthContext.Provider value={{ isloggedin:"manasa"}}>
    <MainNavigation />
        <main>
            <Outlet />
        </main>
    </AuthContext.Provider>
    </MyContext.Provider>
    </CounterProvider>
    </Provider>

    </>
}
export default RootLayout;
