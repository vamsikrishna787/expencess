import { useEffect, useState } from "react";

const useCounter =(forwrds:boolean)=>{
    const [counter,SetCounter]=useState(0);

    if(forwrds){
        SetCounter((currentcounter)=>currentcounter+1);
    }
    else{
        SetCounter((currentcounter)=>currentcounter-1);
    }
return counter;
}

export default useCounter;