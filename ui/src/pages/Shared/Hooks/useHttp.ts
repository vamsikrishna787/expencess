import { useState, useEffect } from 'react';
import axios, { AxiosResponse } from 'axios';

interface HttpsOptions {
  url: string;
  method: 'GET' | 'POST' | 'PUT' | 'DELETE';
  headers?: any;
  data?: any;
}

interface HttpsResult<T> {
  data: T | null;
  error: Error | null;
  isLoading: boolean;
}

const useHttps = <T>(options: HttpsOptions): HttpsResult<T> => {
  const [data, setData] = useState<T | null>(null);
  const [error, setError] = useState<Error | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    const source = axios.CancelToken.source();

    const fetchData = async () => {
      try {
        const response: AxiosResponse<T> = await axios({
          ...options,
         // httpsAgent: new https.Agent({}),
          cancelToken: source.token,
        });
        setData(response.data);
      } catch (error) {
      //  setError(error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchData();

    return () => {
      source.cancel();
    };
  }, [options]);

  return { data, error, isLoading };
};
