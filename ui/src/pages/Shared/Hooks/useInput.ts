import { useState } from 'react';

export const useInput = (validateValue:any,kls:any) => {

console.log(kls)


  const [enteredValue, setEnteredValue] = useState('');
  const [isTouched, setIsTouched] = useState(false);

  const valueIsValid = validateValue(enteredValue);
  const hasError = !valueIsValid && isTouched;

  const valueChangeHandler = (event:any) => {
    setEnteredValue(event.target.value);
  };

  const inputBlurHandler = (event:any) => {
    setIsTouched(true);
  };

  const reset = () => {
    setEnteredValue('');
    setIsTouched(false);
  };

  const sample=()=>{
    console.log("hai")
  }

  return {
    value: enteredValue,
    isValid: valueIsValid,
    hasError,
    valueChangeHandler,
    inputBlurHandler,
    reset,
    sample
  };
};

