import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Button from '@mui/material/Button';
import ReactDOM from 'react-dom';

interface ErrorModalProps {
    showflag : boolean;
    onConfirm: () => void;
  }

  interface BackdropProps {
    val:boolean;
    onConfirm: () => void;
  }

  const Backdrop: React.FC<BackdropProps> = (props) => {
    return<>
    <Dialog
    open={props.val}
    onClose={props.onConfirm}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description"
  >
    <DialogTitle id="alert-dialog-title">
      {"Use Google's location service?"}
    </DialogTitle>
    <DialogContent>
      <DialogContentText id="alert-dialog-description">
        Let Google help apps determine location. This means sending anonymous
        location data to Google, even when no apps are running.
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button onClick={props.onConfirm}>Disagree</Button>
      <Button onClick={props.onConfirm} autoFocus>
        Agree
      </Button>
    </DialogActions>
  </Dialog>

  </>
} 



const Portal: React.FC<ErrorModalProps> = (props) => {

    
return<>

{ReactDOM.createPortal(
       <Backdrop val={props.showflag} onConfirm={props.onConfirm}  />,
        document.getElementById('overlay-root')!
      )}

</>
}
export default Portal;