import React from "react";



export interface MyData {
    name: string;
    age: number;
    email: string;
  }
  

interface MyContextInterface {
  datas: MyData;
  setData: (data: MyData) => void;
}

export const MyContext = React.createContext<MyContextInterface>({
  datas: {
    name: "",
    age: 0,
    email: "",
  },
  setData: () => {},
});
