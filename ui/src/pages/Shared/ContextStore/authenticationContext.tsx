import React, { createContext, useContext, useState } from 'react';

// define a type for the context value
type CounterContextType = {
  count: number;
  increment: () => void;
  decrement: () => void;
};

// create a context with an initial value
export const CounterContext = createContext<CounterContextType>({
  count: 0,
  increment: () => {},
  decrement: () => {},
});



type MyComponentProps = {
  children: React.ReactNode;
};

// create a provider component for the context
export const CounterProvider: React.FC<MyComponentProps> = ({ children }) => {
  const [count, setCount] = useState<number>(0);



  debugger;
  const increment = () =>  setCount(prevCount => prevCount + 1); 
  const decrement = () =>  setCount(prevCount => prevCount + 1);

  const value = { count, increment, decrement };

  return (
    <CounterContext.Provider value={value}>
      {children}
    </CounterContext.Provider>
  );
};