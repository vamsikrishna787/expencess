import axios, { AxiosResponse } from 'axios';
import getConfig from '../../../config';

interface Data {
    id: number;
    name: string;
}
const data = {
    name: 'John',
    email: 'john@example.com'
};



const token = 'your_jwt_token';
axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;


export  class LoginService {
    

    public async getData(): Promise<AxiosResponse<Data[]>> {
        return  await axios.get(`${getConfig()}WeatherForecast`);
    }

    public async postData(): Promise< AxiosResponse<void>> { 
        return await axios.post('https://example.com/api/data', data);
    }
}