interface Config {
  apiUrl: string;
}

const configs: Record<string, Config> = {
  development: {
    apiUrl: 'https://localhost:44362/',
  },
  production: {
    apiUrl: 'https://api.example.com',
  },
};

const getConfig = (): string => {
  const env = process.env.REACT_APP_ENV || 'development';
  return configs[env].apiUrl;
};

export default getConfig;